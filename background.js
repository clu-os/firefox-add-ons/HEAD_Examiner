// HEAD Examiner v0.3.0
// Copyright © 2019–2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

// `options_default` loaded in `manifest.json`

const known_extensions = [
	'Top_Page_Button@firefox.clu-os.net',
	'ToC_Button@firefox.clu-os.net',
	'First_Page_Button@firefox.clu-os.net',
	'Prev_Page_Button@firefox.clu-os.net',
	'Up_Button@firefox.clu-os.net',
	'Next_Page_Button@firefox.clu-os.net',
	'Last_Page_Button@firefox.clu-os.net',
	'Index_Button@firefox.clu-os.net',
	'Glossary_Button@firefox.clu-os.net',
	'Search_Page_Button@firefox.clu-os.net',

	'Feed_Indicator@firefox.clu-os.net',
	'Page_Metadata@firefox.clu-os.net',
	'Twitter_Card_Preview@firefox.clu-os.net',
];
const available_extensions = new Map();

const content_script_match = browser.runtime.getManifest().content_scripts[0].matches;

function sendData(extID, data, tabID) {
	browser.runtime.sendMessage(
		extID,
		{
			'data'  : data,
			'tabId' : tabID
		}
	).catch(
		function(e) {
			console.error('Error sending message to: ' + extID + '; ' + e);
		}
	);
}

function sendLINK(extID, prop, message, tabID) {
	if (
		available_extensions.get(extID)
		&& message.link.hasOwnProperty(prop)
	)
	{
		browser.runtime.sendMessage(
			extID,
			{
				'link'  : message.link[prop],
				'tabId' : tabID
			}
		).catch(
			function(e) {
				console.error('Error sending message to: ' + extID + '; ' + e);
			}
		);
	}
}

function onMessageRuntime(message, sender, sendResponse) {
	const exists_LINK = message.hasOwnProperty('link');
	const exists_META = message.hasOwnProperty('meta');
	const exists_RDFa = message.hasOwnProperty('RDFa');

	const exists_RDFa_og = exists_RDFa && message.RDFa.has('og');

	// Log the generated object from each document scan
	browser.storage.local.get(
		{ 'log_scans' : options_default.log_scans }
	).then(
		function (r) {
			if ( r.log_scans === true )
			{
				console.dir(message);
			}
		}
	)

	// Page_Metadata@firefox.clu-os.net support
	if ( available_extensions.get('Page_Metadata@firefox.clu-os.net') )
	{
		sendData('Page_Metadata@firefox.clu-os.net', message, sender.tab.id);
	}

	// Twitter_Card_Preview@firefox.clu-os.net support
	if ( available_extensions.get('Twitter_Card_Preview@firefox.clu-os.net') )
	{
		const exists_META_twitter = exists_META && message.meta.has('twitter');
		//    exists_RDFa_og declared above

		// See https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/markup

		let og;
		let og_check = false;	// Necessary elements for a card to be generated from `og` alone
		if (exists_RDFa_og)
		{
			og = message.RDFa.get('og');
			og_check = (
				   og.hasOwnProperty('type')
				&& og.hasOwnProperty('title')
				&& og.hasOwnProperty('description')
			);
		}

		// Twitter uses OpenGraph when possible
		let twitter;
		let twitter_check = false;	// Necessary elements for a card to be generated from `twitter` or `twitter` + `og`
		if (exists_META_twitter)
		{
			twitter = message.meta.get('twitter');
			twitter_check = (
				twitter.hasOwnProperty('card')
				&&
				(
					twitter.hasOwnProperty('title')
					||
					(
						   exists_RDFa_og
						&& og.hasOwnProperty('title')
					)
				)
			);
		}

		if (twitter_check || og_check)
		{
			const data = new Object();
			data['url'] = message.url;
			if (exists_META_twitter)
			{
				data['twitter'] = twitter;
			}
			if (exists_RDFa_og)
			{
				data['og'] = og;
			}
			sendData('Twitter_Card_Preview@firefox.clu-os.net', data, sender.tab.id);
		}
	}

	// Feed_Indicator@firefox.clu-os.net support
	if ( available_extensions.get('Feed_Indicator@firefox.clu-os.net') )
	{
		const exists_LINK_dnsPrefetch = exists_LINK && message.link.hasOwnProperty('dns-prefetch');
		const exists_LINK_EditURI     = exists_LINK && message.link.hasOwnProperty('edituri');
		const exists_LINK_feeds       = exists_LINK && message.link.hasOwnProperty('feeds');
		const exists_LINK_wlwmanifest = exists_LINK && message.link.hasOwnProperty('wlwmanifest');
		const exists_META_generator   = exists_META && message.meta.has('generator');
		//    exists_RDFa_og declared above
		const exists_RDFa_article     = exists_RDFa && message.RDFa.has('article');

		if (
			   exists_LINK_dnsPrefetch
			|| exists_LINK_EditURI
			|| exists_LINK_feeds
			|| exists_LINK_wlwmanifest
			|| exists_META_generator
			|| exists_RDFa_og
			|| exists_RDFa_article
		)
		{
			const data = new Object();
			data['url'] = message.url;

			if (exists_LINK_dnsPrefetch)
			{
				data['dns-prefetch'] = message.link['dns-prefetch'];
			}
			if (exists_LINK_EditURI)
			{
				data['edituri'] = message.link.edituri;
			}
			if (exists_LINK_feeds)
			{
				data['feeds'] = message.link.feeds;
			}
			if (exists_LINK_wlwmanifest)
			{
				data['wlwmanifest'] = message.link.wlwmanifest;
			}
			if (exists_META_generator)
			{
				data['generator'] = message.meta.get('generator');
			}
			if (exists_RDFa_og)
			{
				data['og'] = message.RDFa.get('og');
			}
			if (exists_RDFa_article)
			{
				data['article'] = message.RDFa.get('article');
			}

			sendData('Feed_Indicator@firefox.clu-os.net', data, sender.tab.id);
		}
	}

	if (exists_LINK)
	{
		sendLINK( 'Top_Page_Button@firefox.clu-os.net',    'top',      message, sender.tab.id);
		sendLINK( 'ToC_Button@firefox.clu-os.net',         'contents', message, sender.tab.id);
		sendLINK( 'First_Page_Button@firefox.clu-os.net',  'first',    message, sender.tab.id);
		sendLINK( 'Prev_Page_Button@firefox.clu-os.net',   'prev',     message, sender.tab.id);
		sendLINK( 'Up_Button@firefox.clu-os.net',          'up',       message, sender.tab.id);
		sendLINK( 'Next_Page_Button@firefox.clu-os.net',   'next',     message, sender.tab.id);
		sendLINK( 'Last_Page_Button@firefox.clu-os.net',   'last',     message, sender.tab.id);
		sendLINK( 'Index_Button@firefox.clu-os.net',       'index',    message, sender.tab.id);
		sendLINK( 'Glossary_Button@firefox.clu-os.net',    'glossary', message, sender.tab.id);
		sendLINK( 'Search_Page_Button@firefox.clu-os.net', 'search',   message, sender.tab.id);
	}
}

function onExtensionInstalled(info) {
	if ( available_extensions.has(info.id) )
	{
		if (info.enabled)
		{
			available_extensions.set(info.id, true);
		}
		else
		{
			available_extensions.set(info.id, false);
		}
	}
}
function onExtensionUninstalledORDisabled(info) {
	if ( available_extensions.has(info.id) )
	{
		available_extensions.set(info.id, false);
	}
}
function onExtensionEnabled(info) {
	if ( available_extensions.has(info.id) )
	{
		available_extensions.set(info.id, true);
	}
}

//
// Initialization
//

// Determine which of the known_extensions are available and enabled
browser.management.getAll().then(
	function(infoAll) {
		for ( let extension of known_extensions )
		{
			let info = infoAll.find(
				function(element) {
					return element.id === extension;
				}
			);
			if (info !== undefined && info.enabled)
			{
				available_extensions.set(extension, true);
			}
			else
			{
				available_extensions.set(extension, false);
			}
		}
	}
);

// Register all event listeners
browser.management.onInstalled.addListener(onExtensionInstalled);
browser.management.onUninstalled.addListener(onExtensionUninstalledORDisabled);
browser.management.onEnabled.addListener(onExtensionEnabled);
browser.management.onDisabled.addListener(onExtensionUninstalledORDisabled);

browser.runtime.onMessage.addListener(onMessageRuntime);
