// HEAD Examiner v0.3.0
// Copyright © 2021, 2023 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

const options_default = {
	'log_scans' : false
}
