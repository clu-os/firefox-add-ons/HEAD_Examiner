// HEAD Examiner v0.3.0
// Copyright © 2020, 2021, 2023, 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

window.addEventListener(
	'load',
	async function (event) {
		const log_scans = document.getElementById('log_scans');

		//
		// Initialize
		//

		// Load options
		const options_local = await browser.storage.local.get(options_default);
		log_scans.checked = options_local.log_scans;

		// Register event listeners
		log_scans.addEventListener(
			'change',
			async function (event) {
				await browser.storage.local.set(
					{
						'log_scans' : log_scans.checked
					}
				)
			}
		);
	}
);
