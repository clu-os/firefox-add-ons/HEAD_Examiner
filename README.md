# HEAD Examiner

> Extracts metadata from each page's HEAD, and makes it available to other add-ons, so they don't have to scan each page themselves

This add-on scans each page (its [HEAD](https://en.wikipedia.org/wiki/List_of_HTML_tags#Document_head_elements)), extracts metadata, and forwards it to other extensions, which provide an actionable user interface based on the metadata. This way, each page is scanned only once, for multiple extensions, instead of independently, by each extension.

It currently works with the following extensions:

- [Table of Contents Button](https://gitlab.com/clu-os/firefox-add-ons/Buttons_for_Linked_Pages/)
- [First Page Button](https://gitlab.com/clu-os/firefox-add-ons/Buttons_for_Linked_Pages/)
- [Previous Page Button](https://gitlab.com/clu-os/firefox-add-ons/Buttons_for_Linked_Pages/)
- [Next Page Button](https://gitlab.com/clu-os/firefox-add-ons/Buttons_for_Linked_Pages/)
- [Last Page Button](https://gitlab.com/clu-os/firefox-add-ons/Buttons_for_Linked_Pages/)
- [Index Button](https://gitlab.com/clu-os/firefox-add-ons/Buttons_for_Linked_Pages/)

Planned future extensions support:

- Feed (RSS) Indicator
- Twitter/X Card Preview

You can also log the output from each page scan to the extension's console for debugging.

## Install

Install from [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/head_examiner/)

> The minimum required version for this add on is `55.0`.  The minimum requirement for submission to [AMO](https://addons.mozilla.org/) is now `58.0` [\[1\](https://github.com/mozilla/addons-linter/issues/5470).  The code will continue to reflect `55.0`, but the version on AMO will require `58.0`.

## License

Licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) only.
