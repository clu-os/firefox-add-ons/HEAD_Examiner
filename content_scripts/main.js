// HEAD Examiner v0.3.0
// Copyright © 2019–2025 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

if (document.contentType === 'text/html' || document.contentType === 'application/xhtml+xml')
{
	// NOTE: 'use strict' must stay inside this block !!!
	'use strict';

	function createArrayInMap(map, key) {
		if ( ! map.has(key) )
		{
			map.set(key, new Array() );
		}
	}
	function createMapInMap(map, key) {
		if ( ! map.has(key) )
		{
			map.set(key, new Map() );
		}
	}
	function createObjectInMap(map, key) {
		if ( ! map.has(key) )
		{
			map.set(key, new Object() );
		}
	}
	function createArrayInObject(obj, prop) {
		if ( ! obj.hasOwnProperty(prop) )
		{
			obj[prop] = new Array();
		}
	}
	function createMapInObject(obj, prop) {
		if ( ! obj.hasOwnProperty(prop) )
		{
			obj[prop] = new Map();
		}
	}
	function createObjectInObject(obj, prop) {
		if ( ! obj.hasOwnProperty(prop) )
		{
			obj[prop] = new Object();
		}
	}

	function scan() {
		const data = new Object();

		for (node of document.head.children)
		{
			// NOTE: HTML tags are in uppercase, XHTML elements are in lowercase
			switch (node.tagName.toLowerCase())
			{
				case 'meta':
					if (
						node.hasAttribute('name')
						&& ! node.hasAttribute('property')
						&& node.hasAttribute('content')
					)
					{
						createMapInObject(data, 'meta');

						const trimName = node.name.trim().toLowerCase();

						// NOTE: `continue` if `trimName` does not contain ':'
						switch (trimName)
						{
							case 'description':
							case 'generator':
							case 'keywords':
							case 'referrer':
							case 'robots':
							case 'viewport':
								data.meta.set( trimName, node.content.trim() );
								continue;
							case 'author':
							case 'creator':
							case 'publisher':
								createArrayInMap( data.meta, trimName );
								data.meta.get(trimName).push( node.content.trim() );
								continue;
						}

						const splitName = trimName.split(':');

						switch (trimName)
						{
							case 'twitter:card':
							case 'twitter:description':
							case 'twitter:title':
								createObjectInMap( data.meta, splitName[0] );
								data.meta.get( splitName[0] )[ splitName[1] ] = node.content.trim();
								break;
							case 'twitter:site':
							case 'twitter:creator':
							case 'twitter:image':
							case 'twitter:player':
							// Twitter Widgets Webpage Properties
							// https://developer.twitter.com/en/docs/twitter-for-websites/webpage-properties/overview
							case 'twitter:dnt':
								createObjectInMap( data.meta, splitName[0] );
								createObjectInObject( data.meta.get( splitName[0] ), splitName[1] );
								data.meta.get( splitName[0] )[ splitName[1] ]['_value'] = node.content.trim();
								break;
							case 'twitter:site:id':
							case 'twitter:creator:id':
							case 'twitter:image:alt':
							case 'twitter:player:width':
							case 'twitter:player:height':
							case 'twitter:player:stream':
							case 'twitter:app:country':
							// Twitter Widgets Webpage Properties
							// https://developer.twitter.com/en/docs/twitter-for-websites/webpage-properties/overview
							case 'twitter:widgets:autoload':
							case 'twitter:widgets:border-color':
							case 'twitter:widgets:csp':
							case 'twitter:widgets:link-color':
							case 'twitter:widgets:theme':
							// https://blog.twitter.com/official/en_us/a/2016/coming-soon-an-improved-embedded-timeline.html
							case 'twitter:widgets:new-embed-design':
								createObjectInMap( data.meta, splitName[0] );
								createObjectInObject( data.meta.get( splitName[0] ), splitName[1] );
								data.meta.get( splitName[0] )[ splitName[1] ][ splitName[2] ] = node.content.trim();
								break;
							case 'twitter:app:name:iphone':
							case 'twitter:app:id:iphone':
							case 'twitter:app:url:iphone':
							case 'twitter:app:name:ipad':
							case 'twitter:app:id:ipad':
							case 'twitter:app:url:ipad':
							case 'twitter:app:name:googleplay':
							case 'twitter:app:id:googleplay':
							case 'twitter:app:url:googleplay':
								createObjectInMap( data.meta, splitName[0] );
								createObjectInObject( data.meta.get( splitName[0] ), splitName[1] );
								createObjectInObject( data.meta.get( splitName[0] )[ splitName[1] ], splitName[3] );
								data.meta.get( splitName[0] )[ splitName[1] ][ splitName[3] ][ splitName[2] ] = node.content.trim();
								break;
							default:
								switch ( splitName[0] )
								{
									case 'twitter':
										createObjectInMap( data.meta, splitName[0] );
										switch ( splitName[1] )
										{
											case 'widgets':
												createObjectInObject( data.meta.get( splitName[0] ), splitName[1] );
												createArrayInObject( data.meta.get( splitName[0] )[ splitName[1] ], '_unknown_' );
												data.meta.get( splitName[0] )[ splitName[1] ]['_unknown_'].push(
													{
														'name'   : trimName,
														'content': node.content.trim()
													}
												);
												break;
											default:
												createArrayInObject( data.meta.get( splitName[0] ), '_unknown_' );
												data.meta.get( splitName[0] )['_unknown_'].push(
													{
														'name'   : trimName,
														'content': node.content.trim()
													}
												);
												break;
										}
										break;
									default:
										createArrayInMap(data.meta, '_unknown_');
										data.meta.get('_unknown_').push(
											{
												'name'   : trimName,
												'content': node.content.trim()
											}
										);
								}
						}
					}
					else if (
						node.hasAttribute('property')
						&& node.hasAttribute('content')
					)
					{
						// RDFa
						createMapInObject(data, 'RDFa');

						// NOTE: `node.property.trim()` does not work
						const trimProperty = node.attributes.property.value.trim();
						const splitProperty = trimProperty.split(':');

						// Reusable variable in switch statement
						let og_image_array;

						switch (trimProperty)
						{
							case 'article:published_time':
							case 'article:modified_time':
							case 'article:expiration_time':
							case 'article:section':
							case 'og:type':
							case 'og:title':
							case 'og:description':
							case 'og:determiner':
							case 'og:url':
							case 'og:site_name':
								createObjectInMap( data.RDFa, splitProperty[0] );
								data.RDFa.get( splitProperty[0] )[ splitProperty[1] ] = node.content.trim();
								break;
							case 'article:tag':
								createObjectInMap( data.RDFa, splitProperty[0] );
								createArrayInObject( data.RDFa.get(splitProperty[0] ), splitProperty[1] );
								data.RDFa.get( splitProperty[0] )[ splitProperty[1] ] = node.content.trim();
								break;
							case 'og:audio':
							case 'og:audio:url':
							case 'og:image':
							case 'og:image:url':
							case 'og:video':
							case 'og:video:url':
								createObjectInMap( data.RDFa, splitProperty[0] );
								createArrayInObject( data.RDFa.get( splitProperty[0] ), splitProperty[1] );
								og_image_array = data.RDFa.get( splitProperty[0] )[ splitProperty[1] ];
								if (
									0 === og_image_array.length
									||
									og_image_array[og_image_array.length - 1].url !== node.content.trim()
								)
								{
									og_image_array.push(
										{
											'url' : node.content.trim()
										}
									);
								}
								break;
							case 'og:audio:secure_url':
							case 'og:image:secure_url':
							case 'og:video:secure_url':
								createObjectInMap( data.RDFa, splitProperty[0] );
								createArrayInObject( data.RDFa.get( splitProperty[0] ), splitProperty[1] );
								og_image_array = data.RDFa.get( splitProperty[0] )[ splitProperty[1] ];
								if (
									0 === og_image_array.length
									||
									og_image_array[og_image_array.length - 1].url !== node.content.trim()
								)
								{
									og_image_array.push(
										{
											'secure_url' : node.content.trim()
										}
									);
								}
								else
								{
									og_image_array[og_image_array.length - 1]['secure_url'] = node.content.trim();
								}
								break;
							case 'og:audio:type':
							case 'og:image:type':
							case 'og:image:width':
							case 'og:image:height':
							case 'og:image:alt':
							case 'og:video:type':
							case 'og:video:width':
							case 'og:video:height':
							case 'og:video:alt':
								createObjectInMap( data.RDFa, splitProperty[0] );
								createArrayInObject( data.RDFa.get( splitProperty[0] ), splitProperty[1] );
								og_image_array = data.RDFa.get( splitProperty[0] )[ splitProperty[1] ];
								if ( 0 < og_image_array.length )
								{
									og_image_array[og_image_array.length - 1][ splitProperty[2] ] = node.content.trim();
								}
								break;
							case 'og:locale':
								createObjectInMap( data.RDFa, splitProperty[0] );
								createObjectInObject( data.RDFa.get( splitProperty[0] ), splitProperty[1] );
								data.RDFa.get( splitProperty[0] )[ splitProperty[1] ]['_value'] = node.content.trim();
								break;
							case 'og:locale:alternate':
								createObjectInMap( data.RDFa, splitProperty[0] );
								createObjectInObject( data.RDFa.get( splitProperty[0] ), splitProperty[1] );
								createArrayInObject( data.RDFa.get( splitProperty[0] )[ splitProperty[1] ], splitProperty[2] );
								data.RDFa.get( splitProperty[0] )[ splitProperty[1] ][ splitProperty[2] ].push( node.content.trim() );
								break;
							default:
								createArrayInMap(data.RDFa, '_unknown_');
								data.RDFa.get('_unknown_').push(
									{
										'property' : trimProperty,
										'content'  : node.content.trim()
									}
								);
						}
					}
					break;
				case 'link':
					if ( node.hasAttribute('rel') && node.hasAttribute('href') )
					{
						function addIcon(node) {
							createArrayInObject(data.link, 'icon');
							data.link.icon.push(
								{
									'href'  : node.href.trim(),
									'type'  : node.type.trim(),
									'sizes' : node.sizes.value.trim()
								}
							);
						}

						function addObject_href(rel, node) {
							createObjectInObject(data.link, rel);
							data.link[rel]['href'] = node.href.trim();
						}

						function addObject_href_title(rel, node) {
							createObjectInObject(data.link, rel);
							data.link[rel]['href']  = node.href.trim();
							data.link[rel]['title'] = node.title.trim();
						}

						function addObject_href_type(rel, node) {
							createObjectInObject(data.link, rel);
							data.link[rel]['href'] = node.href.trim();
							data.link[rel]['type'] = node.type.trim();
						}

						function addObject_href_title_type(rel, node) {
							createObjectInObject(data.link, rel);
							data.link[rel]['href']  = node.href.trim();
							data.link[rel]['title'] = node.title.trim();
							data.link[rel]['type'] = node.type.trim();
						}

						function addArray_href(rel, node) {
							createArrayInObject(data.link, rel);
							data.link[rel].push(
								{
									'href'  : node.href.trim()
								}
							);
						}

						function addArray_href_title(rel, node) {
							createArrayInObject(data.link, rel);
							data.link[rel].push(
								{
									'href'  : node.href.trim(),
									'title' : node.title.trim()
								}
							);
						}

						function addArray_href_type(rel, node) {
							createArrayInObject(data.link, rel);
							data.link[rel].push(
								{
									'href' : node.href.trim(),
									'type' : node.type.trim()
								}
							);
						}

						function addArray_href_title_type(rel, node) {
							createArrayInObject(data.link, rel);
							data.link[rel].push(
								{
									'href'  : node.href.trim(),
									'title' : node.title.trim(),
									'type'  : node.type.trim()
								}
							);
						}

						createObjectInObject(data, 'link');
						const trimREL = node.rel.trim().toLowerCase();

						const RELs = trimREL.split(/\s+/);

						if (1 === RELs.length)
						{
							switch (trimREL)
							{
								case 'icon':        	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									addIcon(node);
									break;
								// Singletons
								case 'canonical':   	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
								case 'privacy-policy':  	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
								case 'shortlink':   	// HTML5 link type extensions (http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions)
								case 'terms-of-service':	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									addObject_href(trimREL, node);
									break;
								// Singletons - with title
								case 'up':          	// IANA (https://www.iana.org/assignments/link-relations/link-relations.xhtml)
								case 'next':        	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
								case 'help':        	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
								case 'contents':    	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
								case 'glossary':    	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
								case 'index':       	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
									addObject_href_title(trimREL, node);
									break;
								// Singletons - with type
								case 'wlwmanifest': 	// HTML5 link type extensions (http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions)
									addObject_href_type(trimREL, node);
									break;
								// Singletons - with title and type
								case 'edituri':     	// HTML5 link type extensions (http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions)
									addObject_href_title_type(trimREL, node);
									break;
								// Singletons - squash to 'license'
								case 'copyright':   	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
								case 'license':     	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									addObject_href_title('license', node);
									break;
								// Singletons - squash to 'top'
								case 'home':        	// Used by the DocBook, GNU Texinfo and GTK-Doc for HTML documentation
								case 'start':       	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
								case 'top':         	// HTML 3.2 (https://www.w3.org/TR/2018/SPSD-html32-20180315/#link)
									addObject_href_title('top', node);
									break;
								// Singletons - squash to 'first'
								case 'begin':       	// HTML5 link type extensions (http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions)
								case 'first':       	// IANA (https://www.iana.org/assignments/link-relations/link-relations.xhtml)
									addObject_href_title('first', node);
									break;
								// Singletons - squash to 'prev'
								case 'prev':        	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
								case 'previous':    	// HTML 3.2 (https://www.w3.org/TR/2018/SPSD-html32-20180315/#link)
									addObject_href_title('prev', node);
									break;
								// Singletons - squash to 'last'
								case 'end':         	// HTML5 link type extensions (http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions)
								case 'last':        	// IANA (https://www.iana.org/assignments/link-relations/link-relations.xhtml)
									addObject_href_title('last', node);
									break;
								// Singletons - squash to 'archives'
								case 'archive':     	// TEST: Blogs
								case 'archives':    	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
									addObject_href_title('archives', node);
									break;
								// Lists - without title
								case 'dns-prefetch':	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									addArray_href(trimREL, node);
									break;
								// Lists - with title
								case 'appendix':    	// HTML 4.01 (https://www.w3.org/TR/html401/types.html#type-links)
								case 'author':      	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
								case 'me':          	// TEST: https://microformats.org/wiki/rel-me
								case 'tag':         	// TEST: Blogs
									addArray_href_title(trimREL, node);
									break;
								// Multiple meanings - 'search'
								case 'search':      	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									if (
										node.hasAttribute('type')
										&& 'application/opensearchdescription+xml' === node.type.trim()
									)
									{
										// Lists - OpenSearch
										addArray_href_title('opensearch', node);
									}
									else
									{
										// Singleton - Search page
										addObject_href_title('search', node);
									}
									break;
								// Multiple meanings - 'alternate'
								case 'alternate':   	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									if ( node.hasAttribute('hreflang') )
									{
										// Lists - Languages
										createMapInObject(data.link, 'lang');
										data.link.lang.set(
											node.hreflang,
											{
												'href'  : node.href.trim(),
												'title' : node.title.trim()
											}
										);
									}
									else if ( node.hasAttribute('type') )
									{
										switch ( node.type.trim() )
										{
											case 'application/rss+xml':
											case 'application/atom+xml':
												// Lists - Feeds
												addArray_href_title_type('feeds', node);
												break;
											case 'application/json':
												// Lists - JSON data
												addArray_href_type('data', node);
												break;
										}
									}
									else
									{
										// Lists - other
										// TODO: media
										// NOTE: Example: https://wiki.debian.org/UsingQuilt
										addArray_href_title('alternate', node);
									}
									break;
								// Lists - 'stylesheet'
								case 'stylesheet':  	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									// Default 'stylesheet'
									createMapInObject(data.link, 'stylesheet');
									createArrayInMap( data.link.stylesheet, '_default');
									data.link.stylesheet.get('_default').push(
										{
											'href'  : node.href.trim(),
											'media' : node.media.trim(),
											'type'  : node.type.trim()
										}
									);
									break;
								default:
									// Default Unknown
									createArrayInObject(data.link, '_unknown_');
									data.link._unknown_.push(
										{
											'rel'  : trimREL,
											'href' : node.href.trim()
										}
									);
							}
						}
						else
						{
							// Special case - continue iteration if matched exactly
							// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
							if ( 'shortcut' === RELs[0] && 'icon' === RELs[1] )
							{
								addIcon(node);
								continue;
							}

							const RELstr = RELs.sort().toString();

							switch (RELstr)
							{
								// Lists - 'stylesheet'
								case 'alternate,stylesheet':	// HTML Living Standard (https://html.spec.whatwg.org/multipage/links.html#linkTypes)
									// Alternate 'stylesheet'
									createMapInObject(data.link, 'stylesheet');
									const style_title = node.title.trim();
									createArrayInMap( data.link.stylesheet, style_title);
									data.link.stylesheet.get(style_title).push(
										{
											'href'  : node.href.trim(),
											'media' : node.media.trim(),
											'type'  : node.type.trim()
										}
									);
									break;
								default:
									// Default Unknown
									createArrayInObject(data.link, '_unknown_');
									data.link._unknown_.push(
										{
											'rel'  : RELstr,
											'href' : node.href.trim()
										}
									);
							}
						}
					}
					break;
				case 'title':
					data['title'] = node.text.trim();
					break;
			}
		}

		data['url']         = document.URL;
		data['charset']     = document.charset;
		data['contentType'] = document.contentType;

		browser.runtime.sendMessage(data);
	}

	//
	// Initialization
	//

	window.addEventListener(
		'pageshow',
		function (event) {
			scan();
		}
	);
}
